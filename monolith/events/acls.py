import requests
from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY


def get_photo(city, state):
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    headers = {"Authorization": PEXELS_API_KEY}
    resp = requests.get(url, headers=headers)
    data = resp.json()
    return data["photos"][0]["src"]["original"]


def get_weather_data(city, state):
    data = requests.get(
        f"http://api.openweathermap.org/geo/1.0/direct?q={city},{state},US&limit=1&appid={OPEN_WEATHER_API_KEY}"
    ).json()
    lat = data[0]["lat"]
    lon = data[0]["lon"]
    data2 = requests.get(
        f"https://api.openweathermap.org/data/2.5/weather?lat={lat}&lon={lon}&appid={OPEN_WEATHER_API_KEY}&units=imperial"
    ).json()
    return {
        "temp": data2["main"]["temp"],
        "description": data2["weather"][0]["description"],
    }
